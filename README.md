English Version at the end.

[![Build Status](https://travis-ci.org/cpcsdk/gfx2crtc.svg?branch=master)](https://travis-ci.org/cpcsdk/gfx2crtc)

Version Française (French Version)
==================================

gfx2crtc - utilitaire de convertion d'image vers image au format cpc 
----------------------------------------------------------------------

gfx2crtc est un utilitaire qui permet la convertion d'image (format brut
lineaire 8bits par pixel ou PNG 1,2 ou 4 bits) dans un format compatible avec
l'Amstrad CPC et Plus.

Ce logiciel est libre. Vous pouvez le redistribuer ou le modifier sous
les termes de la Licence Publique Rien À Branler, Version 1, tel que
publié par Sam Hocevar. Voir le fichier COPIER pour plus de détail.

Utilisation :
-------------

English Version
===============

gfx2crtc - gfx convert tools to amstrad cpc format
----------------------------------------------------

gfx2crtc is a tool to convert gfx (linear raw 8bits per pixel or
PNG 1,2 or 4 bits) in a compatible format for Amstrad CPC and Plus.

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

Usage :
-------
